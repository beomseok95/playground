package com.example.myplayground.reflection

import org.junit.Test

class ReflectionTest {
    fun isOdd(a: Int) = a % 2 != 0
    fun isOdd(s: String) = s == "brillig" || s == "slithy" || s == "tove"
    val isEmptyStringList: List<String>.() -> Boolean = List<String>::isEmpty

    fun <A, B, C> compose(f: (B) -> C, g: (A) -> B): (A) -> C {
        return { x -> f(g(x)) }
    }

    fun length(s: String) = s.length

    val oddLength = compose(::isOdd, ::length)
    val strings = listOf("a", "ab", "abc")

    @Test
    fun helloReflectionTest() {
        val number = listOf(1, 2, 3, 4)
        println(number.filter(::isOdd))

        val predicate: (String) -> Boolean = ::isOdd
        println(predicate("brillig"))

        val empryList = emptyList<String>()
        println(isEmptyStringList(empryList))

        println(strings.filter(oddLength))

    }

    val x = 1

    @Test
    fun getPropertyTest() {
        println(::x.get())
        println(::x.name)
    }

    var y = 1

    @Test
    fun setPropertyTest() {
        ::y.set(2)
        println(y)
    }

    @Test
    fun lengthTest(){
        val strs = listOf("a", "bc", "def")
        println(strs.map(String::length)
        )
    }

    //[1, 2, 3]


    class Foo

    fun function(factory: () -> Foo) {
        val x: Foo = factory()
    }

    @Test
    fun reperenceConstuctorTest(){
        function (::Foo)
    }
}
