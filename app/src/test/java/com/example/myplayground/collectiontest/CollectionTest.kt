package com.example.myplayground.collectiontest

import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Test

class CollectionTest {

    @Test
    fun allTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")

        Assert.assertEquals(true, fruitList.all { it.length >= 5 })
        Assert.assertEquals(false, fruitList.all { it.contains("apple") })
    }

    @Test
    fun anyTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        Assert.assertEquals(true, fruitList.any { it.length >= 10 })
        Assert.assertEquals(false, fruitList.any { it.contains("waterMelon") })
        Assert.assertEquals(true, fruitList.any { it.contains("apple") })
    }

    @Test
    fun noneTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        Assert.assertEquals(true, fruitList.none { it.length < 5 })
        Assert.assertEquals(true, fruitList.none { it.contains("watermelon") })
    }

    @Test
    fun findTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        Assert.assertEquals("banana", fruitList.find { it.contains("banana") })
        Assert.assertEquals("pineapple", fruitList.find { it.contains("apple") })
//        Assert.assertEquals("apple",fruitList.find { it.contains("apple") }) //false  ,, expect= apple ,actual =pineapple
        Assert.assertEquals("apple", fruitList.findLast { it.contains("apple") })


    }

    @Test
    fun countTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        Assert.assertEquals(2, fruitList.count { it.contains("apple") })
    }

    @Test
    fun mapTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        Assert.assertEquals(110, numberList.map { it * 2 }.sum())
    }

    @Test
    fun flatMapTest() {
        val numberList = listOf(2, 5, 11)
        Assert.assertEquals(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), numberList.flatMap {
            println(it)
            it / 2 until it
        })
    }

    @Test
    fun filterTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        Assert.assertEquals(listOf("pineapple", "apple"), fruitList.filter { it.contains("apple") })
    }

    @Test
    fun zipTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        val priceList = listOf(1500, 1000, 2000, 700, 500, 10000)
        assertEquals(listOf(
            "strawberry is 1500won",
            "grape is 1000won",
            "pineapple is 2000won",
            "banana is 700won",
            "apple is 500won"
        ),
            fruitList.zip(priceList).map { "${it.first} is ${it.second}won" })
    }

    @Test
    fun groupByTest() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        assertEquals(mapOf(
            10 to listOf("strawberry"),
            5 to listOf("grape", "apple"),
            9 to listOf("pineapple"),
            6 to listOf("banana")
        )
            , fruitList.groupBy { it.length })
    }

    @Test
    fun distinctTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)
        assertEquals(listOf(1, 2, 3, 4, 5), numberList.distinct())
        assertEquals(listOf(1, 2), numberList.map { it / 2 }.distinct().filter { it != 0 })
        assertEquals(listOf(1, 2, 4), numberList.distinctBy { it / 2 })
    }

    @Test
    fun takeTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        assertEquals(listOf(1, 2, 3, 4, 5), numberList.take(5))
        assertEquals(listOf(6, 7, 8, 9, 10), numberList.takeLast(5))
        assertEquals(listOf(1, 2, 3), numberList.takeWhile { println(it);println(it / 4);println("----");it / 4 == 0 })
        assertEquals(
            listOf(8, 9, 10),
            numberList.takeLastWhile { println(it);println(it / 4);println("----");it / 4 > 1 })
    }

    @Test
    fun dropTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        assertEquals(listOf(6, 7, 8, 9, 10), numberList.drop(5))
        assertEquals(listOf(1, 2, 3, 4, 5), numberList.dropLast(5))
        assertEquals(listOf(6, 7, 8, 9, 10), numberList.dropWhile { it / 6 == 0 })
        assertEquals(
            listOf(1, 2, 3, 4, 5),
            numberList.dropLastWhile { println(it);println(it / 3);println("----"); it / 3 > 1 })
    }

    @Test
    fun maxTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        val fruitList = listOf("strawberry", "grape", "pineapple", "plum", "banana", "apple")
        assertEquals(10, numberList.max())
        assertEquals("strawberry", fruitList.max())
        assertEquals("strawberry", fruitList.maxBy { println(it);println(it.length);it.length })
        //숫자는 최댓값 영어는, a-z 순으로 마지막값
    }

    @Test
    fun minTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        val fruitList = listOf("strawberry", "grape", "pineapple", "plum", "banana", "apple")
        assertEquals(1, numberList.min())
        assertEquals("apple", fruitList.min())
        assertEquals("plum", fruitList.minBy { it.length })
    }

    @Test
    fun averageTest() {
        val numberList = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        assertEquals(5.5, numberList.average())
    }

    @Test
    fun joinToString() {
        val fruitList = listOf("strawberry", "grape", "pineapple", "banana", "apple")
        assertEquals("strawberry, grape, pineapple, banana, apple", fruitList.joinToString()) //기본적으로 ,
        assertEquals("strawberry grape pineapple banana apple", fruitList.joinToString(" ")) // 공백으로 추가
        assertEquals(
            "strawb!!rry, grap!!, pin!!appl!!, banana, appl!!",
            fruitList.joinToString { it.replace("e", "!!") })
    }
}