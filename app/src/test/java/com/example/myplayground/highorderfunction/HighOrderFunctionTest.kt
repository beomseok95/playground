package com.example.myplayground.highorderfunction

import org.junit.Assert
import org.junit.Test
import java.lang.Exception
import java.util.concurrent.locks.Lock

class HighOrderFunctionTest {

    private fun simpleHOF(sum: (Int, Int) -> Int, a: Int, b: Int): Int = sum(a, b)

    @Test
    fun highOrderFunctionTest() {
        val result = simpleHOF({ x, y -> x + y }, 10, 20)
        Assert.assertEquals(30, result)
        val a =1
    }


    @Test
    fun foldTest() {
        val result = listOf(1, 2, 3).fold(0) { aac, e -> aac + e }
        val a =1
        Assert.assertEquals(6, result)
    }

    @Test
    fun reduceTest() {
        val result = listOf(1, 2, 3).reduce { aac, e -> aac + e }
        val a =1
        Assert.assertEquals(6, result)
    }

    fun <T> lock(lock: Lock, body: () -> T): T {
        val a =1
        lock.lock()
        try {
            return body()
        } finally {
            lock.unlock()
        }
    }

    fun <T, R> List<T>.map(transform: (T) -> R): List<R> {
        val a =1
        val result = arrayListOf<R>()
        for (item in this)
            result.add(transform(item))
        return result
    }


    fun test() {

    }

}