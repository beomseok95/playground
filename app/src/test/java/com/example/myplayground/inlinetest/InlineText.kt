package com.example.myplayground.inlinetest

import junit.framework.Assert
import org.junit.Test

class InlineText {
    private fun printResult(body: (Int, Int) -> Int) = body(10, 5)

    fun sum(a: Int, b: Int) = a + b
    fun subtract(a: Int, b: Int) = a - b

    @Test
    fun printResultSumTest() {
        Assert.assertEquals(15, printResult(::sum))
    }

    @Test
    fun printResultSubtractTest() {
        Assert.assertEquals(5, printResult(::subtract))
    }



}