package com.example.myplayground.rxtest

import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import io.reactivex.schedulers.TestScheduler
import java.util.concurrent.TimeUnit


class RxTest {

    @Test
    fun test() {
        Observable.just(2)   // 2 발행합니다.
            .test()   //Observable을 TestObserver로 변환합니다.
            .assertSubscribed() // 성공적으로 onSubscribe가 호출되었는지 테스트합니다.
            .assertValues(2)  // 결과값이 2인지 확인합니다.
            .assertComplete()   //정상적으로 onComplete가 호출되었는지 테스트합니다.
            .assertNoErrors() //에러없이 끝났는지 확인합니다.
    }

    @Test
    fun schedulerTest() {
        val testScheduler = TestScheduler()

        val testObserver = TestObserver<Long>()

        val minTicker = Observable.interval(1, TimeUnit.MINUTES, testScheduler)

        minTicker.subscribe(testObserver)

        //30초 뒤로 이동
        testScheduler.advanceTimeBy(30, TimeUnit.SECONDS)

        //앞으로 이동했기 때문에 아직 배출이 안된것을 테스트
        testObserver.assertValueCount(0)

        //구독 후 뒤로 70초 이동..
        testScheduler.advanceTimeTo(70, TimeUnit.SECONDS)

        //1분이 지난다음으로 이동했으니 1개가 방출된것을 테스트
        testObserver.assertValueCount(1)

        println("#1 current time = " + testScheduler.now(TimeUnit.SECONDS))

        //구독후 90분 뒤로 이동
        testScheduler.advanceTimeTo(90, TimeUnit.MINUTES)

        println("#2 current time = " + testScheduler.now(TimeUnit.SECONDS))

        //결과값 : 90개가 나오게됩니다.
        testObserver.assertValueCount(90)
    }

    @Test
    fun triggerTest() {
        val s = TestScheduler()

        s.createWorker().schedule { println("Immediate") }
        s.createWorker().schedule({ println("20s") }, 20, TimeUnit.SECONDS)
        s.triggerActions()
        println("Virtual time: " + s.now(TimeUnit.SECONDS))
    }


    @Test
    fun throttleTest() {
        val testObserver = Observable.interval(100, TimeUnit.MILLISECONDS, Schedulers.trampoline())
            .take(10)
            .throttleFirst(500, TimeUnit.MILLISECONDS)
            .doOnNext { println(it) }

        testObserver
            .test()
            .assertResult(0, 6)
    }

    @Test
    fun throttleTest2() {
        val testObserver = Observable.create<String> {emitter ->
            emitter.onNext("red")
            TimeUnit.MILLISECONDS.sleep(300)
            emitter.onNext("yellow")
            TimeUnit.MILLISECONDS.sleep(150)
            emitter.onNext("green")
            TimeUnit.MILLISECONDS.sleep(150)
            emitter.onNext("sky")
            TimeUnit.MILLISECONDS.sleep(150)
            emitter.onNext("deep_sky")
            TimeUnit.MILLISECONDS.sleep(150)
            emitter.onNext("pink")
            TimeUnit.MILLISECONDS.sleep(100)

            emitter.onComplete()
        }
            .throttleFirst(300,TimeUnit.MILLISECONDS)

        testObserver
            .test()
            .assertResult("red","yellow","sky","pink")
    }

    @Test
    fun debounceTest() {
        val testObserver = TestObserver<Int>()

        Observable.create<Int> {
            TimeUnit.MILLISECONDS.sleep(500)
            it.onNext(1)
            TimeUnit.MILLISECONDS.sleep(500)
            it.onNext(2)
            TimeUnit.MILLISECONDS.sleep(500)
            it.onNext(3)
            TimeUnit.MILLISECONDS.sleep(2000)
            it.onNext(4)
            TimeUnit.MILLISECONDS.sleep(2000)
            it.onNext(5)
            it.onComplete()
        }
            .subscribeOn(Schedulers.trampoline())
            .scan { t1: Int, t2: Int -> t1 + t2 }
            .debounce(1, TimeUnit.SECONDS)
            .doOnNext { println(it) }
            .subscribe(testObserver)

        testObserver
            .await()
            .assertResult(6, 10, 15)

    }

    @Test
    fun debounceTest2() {
        val testObserver = TestObserver<String>()

        Observable.create<String> {
            it.onNext("red")
            TimeUnit.MILLISECONDS.sleep(350)
            it.onNext("yellow")
            TimeUnit.MILLISECONDS.sleep(100)
            it.onNext("green")
            TimeUnit.MILLISECONDS.sleep(350)
            it.onNext("blue")
            TimeUnit.MILLISECONDS.sleep(350)

            it.onComplete()
        }
            .debounce(300, TimeUnit.MILLISECONDS)
            .doOnNext { println(it) }
            .subscribe(testObserver)

        testObserver
            .await()
            .assertResult("red", "green", "blue")

    }


}