package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test

class LeapTest {

    @Test
    fun yearNotDivisibleBy4() {
        assertFalse(Year(2015).isLeap)
    }


    @Test
    fun yearDivisibleBy4NotDivisibleBy100() {
        assertTrue(Year(1996).isLeap)
    }


    @Test
    fun yearDivisibleBy100NotDivisibleBy400() {
        assertFalse(Year(2100).isLeap)
    }


    @Test
    fun yearDivisibleBy400() {
        assertTrue(Year(2000).isLeap)
    }

    /*
        class Year(val year: Int) {
            val isLeap = cal()

            private fun cal(): Boolean {
                if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) return true
                if ((year % 4 == 0) && (year % 100 == 0)) return false
                if (year % 4 == 0) return true
                return false
            }
        }*/
    class Year(private val year: Int) {
        val isLeap: Boolean = when {
            isDivisibleBy(400) -> true
            isDivisibleBy(100) -> false
            isDivisibleBy(4) -> true
            else -> false

        }

        private fun isDivisibleBy(divider: Int) = year % divider == 0

    }

}

