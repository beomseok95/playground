package com.example.myplayground.exercise.java;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class TwoferTest {

    private Twofer twofer;

    public class Twofer {
//        public String twofer(String input) {
//            String temp;
//            if (input == null)
//                temp = "you";
//            else
//                temp = input;
//
//            return "One for " + temp + ", one for me.";
//        }

//        public String twofer(String input) {
//            return String.format("One for %s, one for me.", Optional.ofNullable(input).orElse("you"));
//        }


        public String twofer(String input) {
            return String.format("One for %s, one for me.", Optional.ofNullable(input).orElse("you"));
        }
    }

    @Before
    public void setup() {
        twofer = new Twofer();
    }

    @Test
    public void noNameGiven() {
        String input = null;
        String expected = "One for you, one for me.";

        Assert.assertEquals(expected, twofer.twofer(input));
    }

    @Test
    public void aNameGiven() {
        String input = "Alice";
        String expected = "One for Alice, one for me.";

        Assert.assertEquals(expected, twofer.twofer(input));
    }

    @Test
    public void anotherNameGiven() {
        String input = "Bob";
        String expected = "One for Bob, one for me.";

        Assert.assertEquals(expected, twofer.twofer(input));
    }
}