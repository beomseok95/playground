package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class IsogramTest(val input: String, val expectedOutput: Boolean) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: isogram({0})={1}")
        fun data() = listOf(
            arrayOf("", true),
            arrayOf("isogram", true),
            arrayOf("eleven", false),
            arrayOf("subdermatoglyphic", true),
            arrayOf("Alphabet", false),
            arrayOf("thumbscrew-japingly", true),
            arrayOf("six-year-old", true),
            arrayOf("Emily Jung Schwartzkopf", true),
            arrayOf("accentor", false)
        )
    }

    @Test
    fun test() {
        assertEquals(
            expectedOutput,
            Isogram.isIsogram(input)
        )
    }

    object Isogram {
        fun isIsogram(input: String): Boolean =
            input.toLowerCase()
                .filter { it.isLetter() }
                .let { it.toSet().size == it.length }
    }
    /*   fun isIsogram(input: String): Boolean {
           val macher = Pattern.compile("[a-z]").matcher(input.toLowerCase())
           val atozGroup = mutableListOf<String>()
           while (macher.find()) {
               if (!atozGroup.contains(macher.group()))
                   atozGroup.add(macher.group())
               else return false
           }
           return true
       }*/
    /* fun isIsogram(input: String) = input.toLowerCase()
         .filter {char->
             char.isLetter()
         }.let {
             it.toSet().size==it.length
         }
    }*/
}