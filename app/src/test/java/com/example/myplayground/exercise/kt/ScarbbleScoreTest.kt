package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class ScrabbleScoreTest(val input: String, val expectedOutput: Int) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: scoreWord({0})={1}")
        fun data() = listOf(
            arrayOf("a", 1),
            arrayOf("A", 1),
            arrayOf("f", 4),
            arrayOf("at", 2),
            arrayOf("zoo", 12),
            arrayOf("street", 6),
            arrayOf("quirky", 22),
            arrayOf("OxyphenButazone", 41),
            arrayOf("pinata", 8),
            arrayOf("", 0),
            arrayOf("abcdefghijklmnopqrstuvwxyz", 87)
        )
    }

    @Test
    fun test() {
        assertEquals(
            expectedOutput,
            ScrabbleScore.scoreWord(input)
        )
    }

    object ScrabbleScore {
        fun scoreWord(input: String) =
            input.toUpperCase()
                .sumBy {
                    when (it) {
                        in "DG" -> 2
                        in "BCMP" -> 3
                        in "FHVWY" -> 4
                        in "K" -> 5
                        in "JX" -> 8
                        in "QZ" -> 10
                        else -> 1
                    }
                }

/*        val point1 = listOf("A", "E", "I", "U", "O", "L", "N", "R", "S", "T")
        val point2 = listOf("D", "G")
        val point3 = listOf("B", "C", "M", "P")
        val point4 = listOf("F", "H", "V", "W", "Y")
        val point5 = listOf("K")
        val point8 = listOf("J", "X")
        val point10 = listOf("Q", "Z")

        fun scoreWord(input: String): Int {
            var point = 0
            input.map {
                it.toUpperCase()
            }.map {
                if (point1.contains(it.toString()))
                    point += 1
                if (point2.contains(it.toString()))
                    point += 2
                if (point3.contains(it.toString()))
                    point += 3
                if (point4.contains(it.toString()))
                    point += 4
                if (point5.contains(it.toString()))
                    point += 5
                if (point8.contains(it.toString()))
                    point += 8
                if (point10.contains(it.toString()))
                    point += 10
            }
            return point
        }*/
    }

}