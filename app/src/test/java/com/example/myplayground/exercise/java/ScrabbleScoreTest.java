package com.example.myplayground.exercise.java;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ScrabbleScoreTest {

    class Scrabble {
        String input;

        List<Character> score1 = Arrays.asList('A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T');
        List<Character> score2 = Arrays.asList('D', 'G');
        List<Character> score3 = Arrays.asList('B', 'C', 'M', 'P');
        List<Character> score4 = Arrays.asList('F', 'H', 'V', 'W', 'Y');
        List<Character> score5 = Arrays.asList('K');
        List<Character> score8 = Arrays.asList('J', 'X');
        List<Character> score10 = Arrays.asList('Q', 'Z');

        Scrabble(String input) {
            this.input = input;
        }

        public int calculate(char c) {
            if (score1.contains(c))
                return 1;
            else if (score2.contains(c))
                return 2;
            else if (score3.contains(c))
                return 3;
            else if (score4.contains(c))
                return 4;
            else if (score5.contains(c))
                return 5;
            else if (score8.contains(c))
                return 8;
            else
                return 10;
        }

        public int getScore() {
            return input.toUpperCase().chars()
                    .mapToObj(c -> (char) c)
                    .map(this::calculate)
                    .mapToInt(i -> i)
                    .sum();

//            return input.toUpperCase().chars()
//                    .reduce(0, (aac, c) -> {
//                        char character = (char) c;
//                        return aac + calculate(character);
//                    });
        }
    }

    @Test
    public void testALowerCaseLetter() {
        Scrabble scrabble = new Scrabble("a");
        assertEquals(1, scrabble.getScore());
    }

    @Test
    public void testAUpperCaseLetter() {
        Scrabble scrabble = new Scrabble("A");
        assertEquals(1, scrabble.getScore());
    }

    @Test
    public void testAValuableLetter() {
        Scrabble scrabble = new Scrabble("f");
        assertEquals(4, scrabble.getScore());
    }

    @Test
    public void testAShortWord() {
        Scrabble scrabble = new Scrabble("at");
        assertEquals(2, scrabble.getScore());
    }

    @Test
    public void testAShortValuableWord() {
        Scrabble scrabble = new Scrabble("zoo");
        assertEquals(12, scrabble.getScore());
    }

    @Test
    public void testAMediumWord() {
        Scrabble scrabble = new Scrabble("street");
        assertEquals(6, scrabble.getScore());
    }

    @Test
    public void testAMediumValuableWord() {
        Scrabble scrabble = new Scrabble("quirky");
        assertEquals(22, scrabble.getScore());
    }

    @Test
    public void testALongMixCaseWord() {
        Scrabble scrabble = new Scrabble("OxyphenButazone");
        assertEquals(41, scrabble.getScore());
    }

    @Test
    public void testAEnglishLikeWord() {
        Scrabble scrabble = new Scrabble("pinata");
        assertEquals(8, scrabble.getScore());
    }

    @Test
    public void testAnEmptyInput() {
        Scrabble scrabble = new Scrabble("");
        assertEquals(0, scrabble.getScore());
    }

    @Test
    public void testEntireAlphabetAvailable() {
        Scrabble scrabble = new Scrabble("abcdefghijklmnopqrstuvwxyz");
        assertEquals(87, scrabble.getScore());
    }

}
