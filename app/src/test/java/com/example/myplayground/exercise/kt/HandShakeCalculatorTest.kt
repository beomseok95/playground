package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test

class HandshakeCalculatorTest {
    enum class Signal(val sign: String) {
        WINK("wink"),
        DOUBLE_BLINK("double blink"),
        CLOSE_YOUR_EYES("close your eyes"),
        JUMP("jump")
    }

    object HandshakeCalculator {

        /* fun calculateHandshake(n: Int): List<Signal> =
             if (n >= 16)
                 calculateHandshake(n - 16).reversed()
             else if (n >= 8)
                 calculateHandshake(n - 8) + listOf(Signal.JUMP)
             else if (n >= 4)
                 calculateHandshake(n - 4) + listOf(Signal.CLOSE_YOUR_EYES)
             else if (n >= 2)
                 calculateHandshake(n - 2) + listOf(Signal.DOUBLE_BLINK)
             else if (n >= 1)
                 listOf(Signal.WINK)
             else
                 listOf()
*/
/*        private infix fun Int.hasBitSet(bit: Int) = ((this shr bit) and 0x1) == 1

        fun calculateHandshake(number: Int): List<Signal> {
            return mutableListOf<Signal>().apply {
                if (number hasBitSet 0) add(Signal.WINK)
                if (number hasBitSet 1) add(Signal.DOUBLE_BLINK)
                if (number hasBitSet 2) add(Signal.CLOSE_YOUR_EYES)
                if (number hasBitSet 3) add(Signal.JUMP)
                if (number hasBitSet 4) reverse()
            }
        }*/


        /*      private val handshakeCodes = mapOf(
                  0b0001 to Signal.WINK,
                  0b0010 to Signal.DOUBLE_BLINK,
                  0b0100 to Signal.CLOSE_YOUR_EYES,
                  0b1000 to Signal.JUMP
              )
              private val REVERSE = 0b10000

              fun calculateHandshake(flags: Int) = handshakeCodes
                  .map { if (flags.hasFlag(it.key)) it.value else null }
                  .filterNotNull()
                  .let { if (flags.hasFlag(REVERSE)) it.asReversed() else it }

              private fun Int.hasFlag(flag: Int) = // this and flag != 0
                  and(flag) != 0*/


        private const val REVERSE_BITS_POSITION = 4

        fun calculateHandshake(decimalNumber: Int): List<Signal> {
            val result = Signal.values()
                .filter { isBitSet(it.ordinal, decimalNumber) }

            return if (isBitSet(REVERSE_BITS_POSITION, decimalNumber))
                result.reversed() else result
        }

        private fun isBitSet(position: Int, decimalNumber: Int): Boolean =
            ((1 shl position) and decimalNumber > 0)
    }


    @Test
    fun testThatInput1YieldsAWink() {
        assertEquals(
            listOf(Signal.WINK),
            HandshakeCalculator.calculateHandshake(
                1
            )
        )
    }


    @Test
    fun testThatInput2YieldsADoubleBlink() {
        assertEquals(
            listOf(Signal.DOUBLE_BLINK),
            HandshakeCalculator.calculateHandshake(
                2
            )
        )
    }

    @Test
    fun testThatInput4YieldsACloseYourEyes() {
        assertEquals(
            listOf(Signal.CLOSE_YOUR_EYES),
            HandshakeCalculator.calculateHandshake(
                4
            )
        )
    }


    @Test
    fun testThatInput8YieldsAJump() {
        assertEquals(
            listOf(Signal.JUMP),
            HandshakeCalculator.calculateHandshake(
                8
            )
        )
    }

    @Test
    fun testAnInputThatYieldsTwoActions() {
        assertEquals(
            listOf(
                Signal.WINK,
                Signal.DOUBLE_BLINK
            ),
            HandshakeCalculator.calculateHandshake(
                3
            )
        )
    }

    @Test
    fun testAnInputThatYieldsTwoReversedActions() {
        assertEquals(
            listOf(
                Signal.DOUBLE_BLINK,
                Signal.WINK
            ),
            HandshakeCalculator.calculateHandshake(
                19
            )
        )
    }

    @Test
    fun testReversingASingleActionYieldsTheSameAction() {
        assertEquals(
            listOf(Signal.JUMP),
            HandshakeCalculator.calculateHandshake(
                24
            )
        )
    }

    @Test
    fun testReversingNoActionsYieldsNoActions() {
        assertEquals(
            emptyList<String>(),
            HandshakeCalculator.calculateHandshake(
                16
            )
        )
    }

    @Test
    fun testInputThatYieldsAllActions() {
        assertEquals(
            listOf(
                Signal.WINK,
                Signal.DOUBLE_BLINK,
                Signal.CLOSE_YOUR_EYES,
                Signal.JUMP
            ),
            HandshakeCalculator.calculateHandshake(
                15
            )
        )
    }

    @Test
    fun testInputThatYieldsAllActionsReversed() {
        assertEquals(
            listOf(
                Signal.JUMP,
                Signal.CLOSE_YOUR_EYES,
                Signal.DOUBLE_BLINK,
                Signal.WINK
            ),
            HandshakeCalculator.calculateHandshake(
                31
            )
        )
    }

    @Test
    fun testThatInput0YieldsNoActions() {
        assertEquals(
            emptyList<String>(),
            HandshakeCalculator.calculateHandshake(
                0
            )
        )
    }

    @Test
    fun testThatInputWithLower5BitsNotSetYieldsNoActions() {
        assertEquals(
            emptyList<String>(),
            HandshakeCalculator.calculateHandshake(
                32
            )
        )
    }

}
