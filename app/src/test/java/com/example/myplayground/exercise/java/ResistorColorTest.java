package com.example.myplayground.exercise.java;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class ResistorColorTest {
    enum Colors {
        black,
        brown,
        red,
        orange,
        yellow,
        green,
        blue,
        violet,
        grey,
        white;
    }

    public class ResistorColor {

        private int colorCode(String color) {
            return Colors.valueOf(color).ordinal();
        }

        private String[] colors() {
            return Arrays.stream(Colors.values()).map(Enum::name).toArray(String[]::new);
        }
    }

    private ResistorColor resistorColor;

    @Before
    public void setup() {
        resistorColor = new ResistorColor();
    }

    @Test
    public void testBlackColorCode() {
        String input = "black";
        int expected = 0;

        assertEquals(expected, resistorColor.colorCode(input));
    }

    @Test
    public void testWhiteColorCode() {
        String input = "white";
        int expected = 9;

        Assert.assertEquals(expected, resistorColor.colorCode(input));
    }

    @Test
    public void testOrangeColorCode() {
        String input = "orange";
        int expected = 3;

        assertEquals(expected, resistorColor.colorCode(input));
    }

    @Test
    public void testColors() {
        String[] expected = {"black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"};

        assertEquals(expected, resistorColor.colors());
    }

}