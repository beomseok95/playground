package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal
import java.math.RoundingMode

class SpaceAgeTest {


    class SpaceAge(private val ageInSeconds: Long) {

        private val SECONDS_IN_EARTH_YEAR = 31_557_600

        fun onEarth() = ageInSeconds.toYearsOnPlanet(Planet.Earth)
        fun onMercury() = ageInSeconds.toYearsOnPlanet(Planet.Mercury)
        fun onVenus() = ageInSeconds.toYearsOnPlanet(Planet.Venus)
        fun onMars() = ageInSeconds.toYearsOnPlanet(Planet.Mars)
        fun onJupiter() = ageInSeconds.toYearsOnPlanet(Planet.Jupiter)
        fun onSaturn() = ageInSeconds.toYearsOnPlanet(Planet.Saturn)
        fun onUranus() = ageInSeconds.toYearsOnPlanet(Planet.Uranus)
        fun onNeptune() = ageInSeconds.toYearsOnPlanet(Planet.Neptune)

        enum class Planet(val earthYearsPerYear: Double) {
            Earth(earthYearsPerYear = 1.0),
            Mercury(0.2408467),
            Venus(0.61519726),
            Mars(1.8808158),
            Jupiter(11.862615),
            Saturn(29.447498),
            Uranus(84.016846),
            Neptune(164.79132)
        }

        fun Long.toYearsOnPlanet(planet: Planet) =
            BigDecimal(this / (planet.earthYearsPerYear * SECONDS_IN_EARTH_YEAR)).setScale(2,RoundingMode.HALF_EVEN).toDouble()
    }
    /* class SpaceAge(val input: Long) {
         val earthSecond = 31557600f
         fun onEarth(): Double {

             return Math.round(input / earthSecond * 100) / 100.00
         }

         fun onMercury(): Double {
             val mecuryYear = 31557600 * 0.2408467
             return Math.round(input / mecuryYear * 100) / 100.00
         }

         fun onVenus(): Double {
             val venusYear = 31557600 * 0.61519726
             return String.format("%.2f", input / venusYear).toDouble()

         }

         fun onMars(): Double {
             val marsYear = 31557600 * 1.8808158
             return String.format("%.2f", input / marsYear).toDouble()

         }

         fun onJupiter(): Double {
             val jupiterYear = 31557600 * 11.862615
             return String.format("%.2f", input / jupiterYear).toDouble()
         }

         fun onSaturn(): Double {
             val saturnYear = 31557600 * 29.447498
             return Math.round(input / saturnYear * 100) / 100.00
         }

         fun onUranus() = String.format("%.2f", input / (31557600 * 84.016846)).toDouble()

         fun onNeptune() = Math.round(input / (31557600 * 164.79132) * 100) / 100.00
     }*/


    @Test
    fun ageOnEarth() {
        val age = SpaceAge(1000000000)

        assertEquals(31.69, age.onEarth())
    }

    @Test
    fun ageOnMercury() {
        val age = SpaceAge(2134835688)

        assertEquals(280.88, age.onMercury())
    }

    @Test
    fun ageOnVenus() {
        val age = SpaceAge(189839836)

        assertEquals(9.78, age.onVenus())
    }

    @Test
    fun ageOnMars() {
        val age = SpaceAge(2329871239L)

        assertEquals(39.25, age.onMars())
    }


    @Test
    fun ageOnJupiter() {
        val age = SpaceAge(901876382)

        assertEquals(2.41, age.onJupiter())
    }

    @Test
    fun ageOnSaturn() {
        val age = SpaceAge(3000000000L)

        assertEquals(3.23, age.onSaturn())
    }

    @Test
    fun ageOnUranus() {
        val age = SpaceAge(3210123456L)
        assertEquals(1.21, age.onUranus())
    }

    @Test
    fun ageOnNeptune() {
        val age = SpaceAge(8210123456L)

        assertEquals(1.58, age.onNeptune())
    }


}

