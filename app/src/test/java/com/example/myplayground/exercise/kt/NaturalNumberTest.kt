package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test

class NaturalNumberTest {


    enum class Classification {
        DEFICIENT, PERFECT, ABUNDANT
    }

    /*fun classify(naturalNumber: Int): Classification {
        require(naturalNumber > 0)
        val divisors = mutableListOf<Int>().apply {
            for (a in 1..naturalNumber) {
                if (naturalNumber % a == 0)
                    this.add(a)
            }
        }

        return when {
            (divisors.sum() - naturalNumber) == naturalNumber -> Classification.PERFECT
            (divisors.sum() - naturalNumber) >= naturalNumber -> Classification.ABUNDANT
            else -> Classification.DEFICIENT
        }
    }*/
    /* private fun classify(naturalNumber: Int) = naturalNumber
         .apply { require(naturalNumber > 0) }
         .compareTo(naturalNumber.factors.sum())
         .let {
             println("compareTo값 $it")
             when {
                 it < 0 -> Classification.ABUNDANT
                 it == 0 -> Classification.PERFECT
                 else -> Classification.DEFICIENT
             }
         }

     private val Int.factors: List<Int>
         get() = (1..this / 2).fold(emptyList()) { factors, i -> if (this % i == 0) factors.plus(i) else factors }*/


    fun classify(input: Int): Classification =
        input.apply { require(input > 0) }
            .compareTo(input.factors.sum())
            .let {
                when{
                    it < 0 -> Classification.ABUNDANT
                    it == 0 -> Classification.PERFECT
                    else -> Classification.DEFICIENT
                }
            }


    private val Int.factors: List<Int>
        get() = (1..this / 2).fold(emptyList()) { factors, i -> if (this % i == 0) factors.plus(i) else factors }

    @Test
    fun smallPerfectNumberIsClassifiedCorrectly() {
        assertEquals(Classification.PERFECT, classify(6))
    }

    @Test
    fun mediumPerfectNumberIsClassifiedCorrectly() {
        assertEquals(Classification.PERFECT, classify(28))
    }

    @Test
    fun largePerfectNumberIsClassifiedCorrectly() {
        assertEquals(Classification.PERFECT, classify(33550336))
    }

    @Test
    fun smallAbundantNumberIsClassifiedCorrectly() {
        assertEquals(Classification.ABUNDANT, classify(12))
    }

    @Test
    fun mediumAbundantNumberIsClassifiedCorrectly() {
        assertEquals(Classification.ABUNDANT, classify(30))
    }

    @Test
    fun largeAbundantNumberIsClassifiedCorrectly() {
        assertEquals(Classification.ABUNDANT, classify(33550335))
    }

    @Test
    fun smallestPrimeDeficientNumberIsClassifiedCorrectly() {
        assertEquals(Classification.DEFICIENT, classify(2))
    }

    @Test
    fun smallestNonPrimeDeficientNumberIsClassifiedCorrectly() {
        assertEquals(Classification.DEFICIENT, classify(4))
    }

    @Test
    fun mediumNumberIsClassifiedCorrectly() {
        assertEquals(Classification.DEFICIENT, classify(32))
    }

    @Test
    fun largeDeficientNumberIsClassifiedCorrectly() {
        assertEquals(Classification.DEFICIENT, classify(33550337))
    }

    @Test
    fun edgeCaseWithNoFactorsOtherThanItselfIsClassifiedCorrectly() {
        assertEquals(Classification.DEFICIENT, classify(1))
    }

    @Test(expected = RuntimeException::class)
    fun zeroIsNotANaturalNumber() {
        classify(0)
    }

    @Test(expected = RuntimeException::class)
    fun negativeNumberIsNotANaturalNumber() {
        classify(-1)
    }

}
