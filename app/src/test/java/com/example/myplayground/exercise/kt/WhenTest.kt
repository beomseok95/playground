package com.example.myplayground.exercise.kt

import org.junit.Assert
import org.junit.Test

class WhenTest {

    fun whenInListCheck(input:String) = when (input) {
        in "ACB" -> true
        else -> false
    }

    @Test
    fun whenFunctionTest(){
        Assert.assertEquals(true ,whenInListCheck("A"))
    }
}