package com.example.myplayground.exercise.java;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;


public class HammingTest {

    class Hamming {
        private String dna, rna;

        Hamming(String dna, String rna) {
            validationCheck(dna, rna);

            this.dna = dna;
            this.rna = rna;
        }

        private void validationCheck(String dna, String rna) {
            if (dna.isEmpty() && rna.isEmpty())
                return;

            if (dna.isEmpty())
                throw new IllegalArgumentException("left strand must not be empty.");

            if (rna.isEmpty())
                throw new IllegalArgumentException("right strand must not be empty.");

            if (dna.length() != rna.length())
                throw new IllegalArgumentException("leftStrand and rightStrand must be of equal length.");
        }

//        public int getHammingDistance() {
////            int count = 0;
////            for (int i = 0; i < dna.length(); i++) {
////                if (dna.charAt(i) != rna.charAt(i))
////                    count++;
////            }
////
////            return count;
////        }

//        public int getHammingDistance() {
//            return IntStream.
//                    range(0, dna.length())
//                    .map(i -> dna.charAt(i) != rna.charAt(i) ? 1 : 0)
//                    .sum();
//        }


        public int getHammingDistance() {
            return (int) IntStream.range(0, dna.length())
                    .filter(i -> dna.charAt(i) != rna.charAt(i))
                    .count();
        }
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testNoDistanceBetweenEmptyStrands() {
        assertEquals(0, new Hamming("", "").getHammingDistance());
    }

    @Test
    public void testNoDistanceBetweenShortIdenticalStrands() {
        assertEquals(0, new Hamming("A", "A").getHammingDistance());
    }

    @Test
    public void testCompleteDistanceInSingleLetterDifferentStrands() {
        assertEquals(1, new Hamming("G", "T").getHammingDistance());
    }

    @Test
    public void testDistanceInLongIdenticalStrands() {
        assertEquals(0, new Hamming("GGACTGAAATCTG", "GGACTGAAATCTG").getHammingDistance());
    }

    @Test
    public void testDistanceInLongDifferentStrands() {
        assertEquals(9, new Hamming("GGACGGATTCTG", "AGGACGGATTCT").getHammingDistance());
    }

    @Test
    public void testValidatesFirstStrandNotLonger() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("leftStrand and rightStrand must be of equal length.");

        new Hamming("AATG", "AAA");
    }

    @Test
    public void testValidatesSecondStrandNotLonger() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("leftStrand and rightStrand must be of equal length.");

        new Hamming("ATA", "AGTG");
    }

    @Test
    public void testDisallowLeftEmptyStrand() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("left strand must not be empty.");

        new Hamming("", "G");
    }

    @Test
    public void testDisallowRightEmptyStrand() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("right strand must not be empty.");

        new Hamming("G", "");
    }

}
