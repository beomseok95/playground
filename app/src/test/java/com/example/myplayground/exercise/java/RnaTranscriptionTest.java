package com.example.myplayground.exercise.java;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import kotlin.jvm.internal.PropertyReference0Impl;

import static org.junit.Assert.assertEquals;

public class RnaTranscriptionTest {
    class RnaTranscription {
//        public String transcribe(String dna) {
//            return Stream.of(dna.split(""))
//                    .map(this::trans)
//                    .collect(Collectors.joining());
//        }

        public String transcribe(String dna) {
            return dna.chars()
                    .mapToObj(c -> (char) c)
                    .map(this::trans)
                    .map(String::valueOf)
                    .collect(Collectors.joining());
        }

        private String trans(String input) {
            if (input.equals("G"))
                return "C";
            else if (input.equals("C"))
                return "G";
            else if (input.equals("T"))
                return "A";
            else if (input.equals("A"))
                return "U";
            else
                return "";
        }

        private Character trans(Character input) {
            if (input.equals('G'))
                return 'C';
            else if (input.equals('C'))
                return 'G';
            else if (input.equals('T'))
                return 'A';
            else if (input.equals('A'))
                return 'U';
            else
                return ' ';
        }
    }

    private RnaTranscription rnaTranscription;

    @Before
    public void setUp() {
        rnaTranscription = new RnaTranscription();
    }

    @Test
    public void testEmptyRnaSequence() {
        assertEquals("", rnaTranscription.transcribe(""));
    }

    @Test
    public void testRnaTranscriptionOfCytosineIsGuanine() {
        assertEquals("G", rnaTranscription.transcribe("C"));
    }

    @Test
    public void testRnaTranscriptionOfGuanineIsCytosine() {
        assertEquals("C", rnaTranscription.transcribe("G"));
    }

    @Test
    public void testRnaTranscriptionOfThymineIsAdenine() {
        assertEquals("A", rnaTranscription.transcribe("T"));
    }

    @Test
    public void testRnaTranscriptionOfAdenineIsUracil() {
        assertEquals("U", rnaTranscription.transcribe("A"));
    }

    @Test
    public void testRnaTranscription() {
        assertEquals("UGCACCAGAAUU", rnaTranscription.transcribe("ACGTGGTCTTAA"));
    }

}


