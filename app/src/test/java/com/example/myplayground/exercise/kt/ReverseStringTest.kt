package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test

class ReverseStringTest {

    fun reverse(input: String) = input.reversed()
//    fun reverse(inputString: String) = buildString {
//        inputString.indices.forEach { this.append(inputString[inputString.length - 1 - it]) }
//    }

    @Test
    fun testAnEmptyString() {
        assertEquals("", reverse(""))
    }

    @Test
    fun testAWord() {
        assertEquals("tobor", reverse("robot"))
    }

    @Test
    fun testACapitalizedWord() {
        assertEquals("nemaR", reverse("Ramen"))
    }

    @Test
    fun testASentenceWithPunctuation() {
        assertEquals("!yrgnuh m'I", reverse("I'm hungry!"))
    }

    @Test
    fun testAPalindrome() {
        assertEquals("racecar", reverse("racecar"))
    }

}