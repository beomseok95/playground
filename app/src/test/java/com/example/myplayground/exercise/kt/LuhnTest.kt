package com.example.myplayground.exercise.kt

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class LuhnTest {
    /*object com.example.myplayground.exercise.kt.Luhn {
        fun isValid(num: String): Boolean {
            if (num.length <= 1) return false
            val p = Pattern.compile("(^[0-9]*$)")
            if (!p.matcher(num).find()) return false
            if (num.replace(" ","").map { it.toString().toInt() }.sum() == 0) return false

            val even = num
                .replace(" ", "")
                .reversed()
                .filterIndexed { index, c ->
                    index % 2 != 0
                }
                .map {
                    it.toString().toInt() * 2
                }
                .map { if (it > 9) it - 9 else it }


            val odd = num
                .replace(" ", "")
                .reversed()
                .filterIndexed { index, c ->
                    index % 2 == 0
                }
                .map { it.toString().toInt() }

            val result = even + odd

            val sums = result.map { it }.sum()
            val final = sums % 10
            return final == 0
        }
    }*/

    @Test
    fun singleDigitStringsCannotBeValid() {
        assertFalse(Luhn.isValid("1"))
    }

    @Test
    fun singleZeroIsInvalid() {
        assertFalse(Luhn.isValid("0"))
    }

    @Test
    fun simpleValidSINThatRemainsValidIfReversed() {
        assertTrue(Luhn.isValid("059"))
    }

    @Test
    fun simpleValidSINThatBecomesInvalidIfReversed() {
        assertTrue(Luhn.isValid("59"))
    }

    @Test
    fun validCanadianSIN() {
        assertTrue(Luhn.isValid("055 444 285"))
    }

    @Test
    fun invalidCanadianSIN() {
        assertFalse(Luhn.isValid("055 444 286"))
    }

    @Test
    fun invalidCreditCard() {
        assertFalse(Luhn.isValid("8273 1232 7352 0569"))
    }

    @Test
    fun validStringsWithNonDigitIncludedBecomeInvalid() {
        assertFalse(Luhn.isValid("055a 444 285"))
    }

    @Test
    fun validStringsWithPunctuationIncludedBecomeInvalid() {
        assertFalse(Luhn.isValid("055-444-285"))
    }

    @Test
    fun validStringsWithSymbolsIncludedBecomeInvalid() {
        assertFalse(Luhn.isValid("055£ 444$ 285"))
    }

    @Test
    fun singleZeroWithSpaceIsInvalid() {
        assertFalse(Luhn.isValid(" 0"))
    }

    @Test
    fun moreThanSingleZeroIsValid() {
        assertTrue(Luhn.isValid("0000 0"))
    }

    @Test
    fun inputDigit9IsCorrectlyConvertedToOutputDigit9() {
        assertTrue(Luhn.isValid("091"))
    }

    @Test
    fun stringsWithNonDigitsIsInvalid() {
        assertFalse(Luhn.isValid(":9"))
    }

}

/*object com.example.myplayground.exercise.kt.Luhn {
    fun isValid(number: String): Boolean =
        with(number.replace(" ", "")) {
            matches("[0-9]{2,}".toRegex()) && reversed().map { it.toString().toInt() }
                .mapEvenPositions {
                    (it * 2)
                        .let {
                            if (it > 9)
                                it - 9
                            else
                                it
                        }
                }
                .sum()
                .rem(10) == 0
        }
}

private fun <T> Iterable<T>.mapEvenPositions(transform: (T) -> T): Iterable<T> =
    mapIndexed { index, value -> if (index.isOdd) transform(value) else value }

private val Int.isOdd: Boolean get() = this.rem(2) == 1*/

object Luhn {
    fun isValid(s: String): Boolean =
        if (!s.isLegal())
            false
        else
            s.filter { it.isDigit() }
                .map { it.toString().toInt() }
                .reversed()
                .mapIndexed(this::doubleAndNomalize)
                .sum() % 10 == 0


    fun doubleEven(index: Int, e: Int) = if (index % 2 == 1) e * 2 else e
    fun nomalize(e: Int) = if (e > 9) e - 9 else e
    fun doubleAndNomalize(index: Int, e: Int) = nomalize(
        doubleEven(
            index,
            e
        )
    )
    fun String.isLegal() = this.filter { it.isDigit() }.length > 1 && this.all { it.isDigit() || it == ' ' }

}

















