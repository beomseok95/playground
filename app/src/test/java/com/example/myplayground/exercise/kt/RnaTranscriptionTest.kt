package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Test

class RnaTranscriptionTest {
    /*    private fun transcribeToRna(dna: String)= buildString {
            dna.iterator().forEach {
                if(it.toString()=="C") append("G")
                if(it.toString()=="G") append("C")
                if(it.toString()=="T") append("A")
                if(it.toString()=="A") append("U")
            }
        }*/
    /* private fun transcribeToRna(dna: String): String = dna.map {
         println(it)
         when (it) {
             'G' -> 'C'
             'C' -> 'G'
             'T' -> 'A'
             'A' -> 'U'
             else -> it
         }
         println(it)
         println("------------------------")
     }.joinToString(separator = "")*/

    /*val dnaToRna = mapOf('C' to 'G', 'G' to 'C', 'T' to 'A', 'A' to 'U')
    fun transcribeToRna(input:String):String =input.map { dnaToRna[it] }.joinToString(separator = "")*/


    /* fun trans(nucleotide: Char) =
         when (nucleotide) {
             'G' -> 'C'
             'C' -> 'G'
             'T' -> 'A'
             else -> 'U'
         }

     fun transcribeToRna(dna: String) =
         dna.fold("") { rna, nucleotide -> rna + trans(nucleotide.toUpperCase()) }*/


    fun transcribeToRna(input: String): String {
//        return input.map { it }
//            .map(this::trans)
//            .joinToString("")


//       return  input.map(::trans).joinToString("")


        return input.fold("") { aac, c -> aac + trans(c) }
    }

    fun trans(c: Char): Char {
        return when (c) {
            'G' -> 'C'
            'C' -> 'G'
            'T' -> 'A'
            'A' -> 'U'
            else -> ' '
        }
    }


    @Test
    fun cytosineComplementIsGuanine() {
        assertEquals("G", transcribeToRna("C"))
    }

    @Test
    fun guanineComplementIsCytosine() {
        assertEquals("C", transcribeToRna("G"))
    }

    @Test
    fun thymineComplementIsAdenine() {
        assertEquals("A", transcribeToRna("T"))
    }

    @Test
    fun adenineComplementIsUracil() {
        assertEquals("U", transcribeToRna("A"))
    }

    @Test
    fun rnaTranscription() {
        assertEquals("UGCACCAGAAUU", transcribeToRna("ACGTGGTCTTAA"))
    }

}
