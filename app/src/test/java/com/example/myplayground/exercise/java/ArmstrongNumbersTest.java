package com.example.myplayground.exercise.java;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ArmstrongNumbersTest {
    class ArmstrongNumbers {
        //        public boolean isArmstrongNumber(int input) {
//            String s = String.valueOf(input);
//
//            int result = Stream.of(s.split(""))
//                    .mapToInt(Integer::parseInt)
//                    .map(atom -> (int) Math.pow(atom, s.length()))
//                    .peek(System.out::println)
//                    .sum();
//
//
//            return input == result;
//        }
       /* public boolean isArmstrongNumber(int input) {
            String s = String.valueOf(input);

            return input == s.chars()
                    .parallel()
                    .map(Character::getNumericValue)
                    .map(d -> (int) Math.pow(d, s.length()))
                    .sum();
        }*/


        public boolean isArmstrongNumber(int input) {

            String inputString = String.valueOf(input);

            return input == inputString.chars()
                    .parallel()
                    .map(Character::getNumericValue)
                    .map(c -> (int) Math.pow(c, inputString.length()))
                    .sum();
        }
    }

    private ArmstrongNumbers armstrongNumbers;

    @Before
    public void setup() {
        armstrongNumbers = new ArmstrongNumbers();
    }

    @Test
    public void zeroIsArmstrongNumber() {
        int input = 0;

        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void singleDigitsAreArmstrongNumbers() {
        int input = 5;

        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void noTwoDigitArmstrongNumbers() {
        int input = 10;

        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void threeDigitNumberIsArmstrongNumber() {
        int input = 153;

        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void threeDigitNumberIsNotArmstrongNumber() {
        int input = 100;

        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void fourDigitNumberIsArmstrongNumber() {
        int input = 9474;

        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void fourDigitNumberIsNotArmstrongNumber() {
        int input = 9475;

        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void sevenDigitNumberIsArmstrongNumber() {
        int input = 9926315;

        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void sevenDigitNumberIsNotArmstrongNumber() {
        int input = 9926314;

        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

}
