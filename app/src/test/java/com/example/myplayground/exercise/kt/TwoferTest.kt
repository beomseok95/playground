package com.example.myplayground.exercise.kt

import junit.framework.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test

class TwoferTest {

    @Test
    fun noNameGiven() {
        assertEquals("One for you, one for me.", twofer())
    }

    @Test
    fun aNameGiven() {
        assertEquals("One for Alice, one for me.", twofer("Alice"))
    }

    @Test
    fun anotherNameGiven() {
        assertEquals("One for Bob, one for me.", twofer("Bob"))
    }

    @Test
    fun emptyStringGiven() {
        assertEquals("One for , one for me.", twofer(""))
    }

    /*
        private fun twofer(name: String): String {
            return "One for $name, one for me."
        }
        private fun twofer():String{
            return "One for you, one for me."
        }

    */
    private fun twofer(name: String = "you") = "One for $name, one for me."

}