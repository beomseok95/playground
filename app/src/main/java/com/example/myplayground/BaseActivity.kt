package com.example.myplayground

import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import org.jetbrains.anko.toast

open class BaseActivity : AppCompatActivity(), DisposeOnDestroy {
    private val compositeDisposable = CompositeDisposable()
    override fun addDisposable(disposable: Disposable): Boolean {
        return compositeDisposable.add(disposable)
    }

    override fun clearDisposables() {
        compositeDisposable.clear()
        toast(compositeDisposable.isDisposed.toString())
    }

    override fun onDestroy() {
        super.onDestroy()
        clearDisposables()
    }
}