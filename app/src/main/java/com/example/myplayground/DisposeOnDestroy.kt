package com.example.myplayground

import io.reactivex.disposables.Disposable

interface DisposeOnDestroy {
    fun addDisposable(disposable: Disposable): Boolean
    fun clearDisposables()
}