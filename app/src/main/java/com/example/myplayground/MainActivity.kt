package com.example.myplayground

import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.toObservable
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import java.util.concurrent.TimeUnit

class MainActivity : BaseActivity() {
    val TAG = "MainActivity"
    val disposabe = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /**
         *메모리캐시 vs 디스크캐시
         * 메모리캐시는 처리속도가 빠르지만 저장공간이 작음
         * 디스크캐시는 저장공간은 상대적으로 크지만, 입출력으로 인한 속도처리가 느림
         *
         * 메모리 캐시는 메인스레드에서 진행
         * 디스크캐시는 워크스레드에서 진행
         */

        Observable.interval(100, TimeUnit.MILLISECONDS)
            .filter { it <= 100 }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                progress_circular.curValue = it.toInt()
            }, {

            })
            .addTo(disposabe)


        preloadBtn.setOnClickListener {
            val url = "http://test-img.next-p.co.kr/banner/A296911I2100000_01.png"
            Glide.with(this)
                .load(url)
                .preload()//  디스크캐시에 로드?

        }
        cacheClearBtn.setOnClickListener {
            Thread(Runnable {
                Glide.get(this)
                    .clearDiskCache() //workThread
            }).start()  //디스크캐시 초기화
        }
        /* preloadBtn.setOnClickListener {
             val url = "http://test-img.next-p.co.kr/banner/A296911I2100000_01.png"
             Glide.with(this)
                 .asBitmap()
                 .load(url)
                 .listener(object : RequestListener<Bitmap> {
                     override fun onLoadFailed(
                         e: GlideException?,
                         model: Any?,
                         target: Target<Bitmap>?,
                         isFirstResource: Boolean
                     ): Boolean {
                         Log.e("MainActivity", "실패")
                         return false
                     }

                     override fun onResourceReady(
                         resource: Bitmap?,
                         model: Any?,
                         target: Target<Bitmap>?,
                         dataSource: DataSource?,
                         isFirstResource: Boolean
                     ): Boolean {
                         bitmapImage = resource!!
                         Log.e("MainActivity", "성공")
                         return false
                     }
                 }).submit()
         }

         cacheClearBtn.setOnClickListener {

             bitmapImage?.recycle()
             bitmapImage = null
         }*/


        textView.setOnClickListener {
            startActivity<Main2Activity>()
        }
    }

    override fun onDestroy() {
        disposabe.clear()
        super.onDestroy()
    }

    companion object {
        var bitmapImage: Bitmap? = null
    }

}
