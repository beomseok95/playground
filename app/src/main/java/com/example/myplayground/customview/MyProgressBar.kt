package com.example.myplayground.customview

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.example.myplayground.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit
import android.graphics.RectF
import android.util.Log


class MyProgressBar : View {
    private val disposable = CompositeDisposable()
    var lineColor: Int = 0
    var maxValue: Int = 0
    var curValue: Int = 0

    constructor(context: Context) : super(context, null)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs, 0) {
        lineColor = context.obtainStyledAttributes(attrs, R.styleable.ProgressBar)
            .getColor(R.styleable.ProgressBar_lineColor, Color.RED)
        maxValue = context.obtainStyledAttributes(attrs, R.styleable.ProgressBar)
            .getColor(R.styleable.ProgressBar_maxValue, 100)
        curValue = context.obtainStyledAttributes(attrs, R.styleable.ProgressBar)
            .getColor(R.styleable.ProgressBar_curValue, 0)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr)


    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        Log.d("bsjo","bsjo onAttachedToWindow")
    }


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        Log.d("bsjo","bsjo onMeasure")

        //2
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        //3
        val width = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> (paddingLeft + paddingRight + 200)
            else -> widthMeasureSpec
        }

        val height = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> (paddingTop + paddingBottom + 200)
            else -> heightMeasureSpec
        }

        setMeasuredDimension(width, height)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        Log.d("bsjo","bsjo onLayout")

    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        Log.d("bsjo","bsjo dispatchDraw")
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        Log.d("bsjo","bsjo onDraw")

        val width = measuredWidth + 0.0f
        val height = measuredHeight + 0.0f

        val circle = Paint()
        circle.color = this.lineColor
        circle.strokeWidth = 10f
        circle.isAntiAlias = false
        circle.style = Paint.Style.STROKE

        canvas?.drawArc(
            RectF(
                10f, 10f, width - 10f, height - 10f
            ), -90f,
            (this.curValue + 0.0f) / (this.maxValue + 0.0f) * 360, false, circle
        )

        val textp = Paint()
        textp.color = Color.BLACK
        textp.textSize = 30f
        textp.textAlign = Paint.Align.CENTER


        if (System.currentTimeMillis() / 1000 % 2 == 0L) {
            canvas?.drawText(
                "${this.curValue} / ${this.maxValue}",
                (width / 2),
                (height / 2),
                textp
            )
        }

        Observable.interval(1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                invalidate()
            }, {

            })
            .addTo(disposable)
    }


    override fun onDetachedFromWindow() {
        disposable.clear()
        Log.d("bsjo","bsjo onDetachedFromWindow")
        super.onDetachedFromWindow()
    }
}