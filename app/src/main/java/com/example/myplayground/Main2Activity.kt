package com.example.myplayground

import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.myplayground.MainActivity.Companion.bitmapImage
import io.reactivex.Observable
import io.reactivex.rxkotlin.toObservable
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : BaseActivity() {
    val TAG = "Main2Activity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        //val url = "http://test-img.next-p.co.kr/banner/A296911I2100000_01.png"
        val url = "./storage/emulated/0/Android/data/kr.nextm.dev/cache/event/image.png"
        Glide.with(this)
            .load(url)
            .apply(
                RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL) //디스크캐시 사용
                    .skipMemoryCache(true) //메모리캐시 사용하지않음
            )
            .into(imageView)


        //   imageView.setImageBitmap(bitmapImage)

        listOf("범석", "현순", "성식", "형우", "동한")
            .toObservable()
            .flatMap {
                Observable.just("정$it")
            }.subscribe {
                Log.e(TAG, it)
            }.let { addDisposable(it) }
    }
}
