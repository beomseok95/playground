package com.example.myplayground

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitAdapter {

    private val BASE_URL = "https://test-13mile-android.herokuapp.com/"
    val instance = RetrofitAdapter
    fun getServiceApi(): ApiService {
        var logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY

        var client = OkHttpClient().newBuilder()
            .addInterceptor(logInterceptor)
            .build()

        var retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .baseUrl(BASE_URL)
            .build()
        return retrofit.create(ApiService::class.java)

    }

}