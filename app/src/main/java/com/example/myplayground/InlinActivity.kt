package com.example.myplayground

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;

import kotlinx.android.synthetic.main.activity_main3.*

class InlinActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        callFunction()
    }
    inline fun doSometing(body: () -> Unit) {
        body()
    }
    fun callFunction(){
        doSometing { println("문자열출력!") }
    }


    inline fun callingLmabda(aLambda:()-> Unit,
                             noinline dontInlineLambda:()->Unit,
                             aLambd2a: () -> Unit){
        /////////
        //내부로직
        ////////

    }

}
